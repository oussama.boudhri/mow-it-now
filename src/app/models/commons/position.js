
export default class Position {
    constructor(x, y) {
        this._x = x
        this._y = y
    }

    set pos_x(value) {
        this._x = value
    }

    get pos_x() {
        return this._x
    }

    set pos_y(value) {
        this._y = value
    }

    get pos_y() {
        return this._y
    }

    getXY() {
        return [this._x, this._y]
    }
}
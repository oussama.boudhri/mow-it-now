import Direction from './direction' 

export default class Mower {
    constructor(position, direction) {
        this._position = position 
        this._direction = direction
    }
    
    get position() {
        return this._position
    }

    set position(value) {
        this._position = value
    }

    get direction() {
        return this._direction
    }

    set direction(value) {
        this._direction = value
    }

}
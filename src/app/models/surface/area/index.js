import State from './state'

export default class Area {
    constructor(position, state) {
        this._position = position
        this._state = State.FREE
    }

    get position() {
        return this._position
    }

    set position(value) {
        this._position = value
    }

    get state() {
        return this._state
    }

    set state(value) {
        this._state = value
    }
}
import Area from './area'
import Position from '../commons/position'

export default class Surface {
    constructor(width, height) {
        this._width = width
        this._height = height
        this.initAreas()
    }

    set width(value) {
        this._width = value
    } 

    get width() {
        return this._width
    }
    
    set height(value) {
        this._height = value
    } 

    get height() {
        return this._height
    }

    get areas() {
        return this._areas
    }

    mostTopRightCoordinate() {
        return [this._width, this._height]
    }

    initAreas() {
        let areas = []
        for (let i = 0; i <= this._width; i++) {
            for (let j = 0; j <= this._width; j++) {
                areas.push(new Area(new Position(i, j)))
            }
        }
        this._areas = areas
    }
}
import Surface from '../models/surface'
import State from '../models/surface/area/state'
import Mower from '../models/mower'
import Position from '../models/commons/position'
import MowerDriver from './mower_driver'
import Direction from '../models/mower/direction'
import logger from 'winston'

export default class InstructionProcessor {
    constructor(instructions){
        this._instructions = instructions
    }

    get instructions() {
        return this._instructions
    }

    set instructions(value) {
        this._instructions = value
    }

    get mowers() {
        return this._mowers
    }

    get surface() {
        return this._surface
    }

    processInstructions() {
        return new Promise(() => {
            this.initSurface()
            .then((surface) => { 
                this._surface = surface
                return this.initMowers()})
            .then((mowers) => {
                this._mowers = mowers
                return this.initActions()
            })
            .then((actions) => {
                return Promise.all(actions)
            })
            .then((mowers) => {

            })
        })
    }

    initSurface() {
        return new Promise((resolve) => {
            logger.info("InstructionProcessor: calling init surface")
            const init_surface_cmd = this._instructions.filter((instruction) => { return instruction.type === "init_surface" })[0]
            
            resolve(new Surface(init_surface_cmd.coordinates.x, init_surface_cmd.coordinates.y))
        })
    }

    initMowers() {
        return new Promise((resolve) => {
            logger.info("InstructionProcessor: calling init mowers")
            const init_mowers_cmd = this._instructions.filter((instruction) => { return instruction.type === "init_mower" })
            let mowers = []
            for (let i = 0; i < init_mowers_cmd.length; i++) {
                const position = new Position(init_mowers_cmd[i].coordinates.x, init_mowers_cmd[i].coordinates.y)
                mowers.push(new Mower(position, init_mowers_cmd[i].coordinates.direction))
                this.updateStateAreas(position, position)
            }
            resolve(mowers)
        })
    }

    initActions() {
        return new Promise((resolve) => {
            logger.info("InstructionProcessor: calling init actions")
            const actions = this._instructions.filter((instruction) => { return instruction.type === "action" })
            const promises = [] 
            for (let i = 0; i < actions.length; i++) {
                promises.push(new Promise(() => { return this.processAction(actions[i]) }))
            }
            resolve(promises)     
        })
    }

    processAction(action) {
        return new Promise((resolve) => { 
            let mower = this.mowers[action.mower_index]
            let driver = new MowerDriver(mower)
            let keys = action.commands.split('')  
            for(let i = 0; i < keys.length; i++) {
                if(keys[i] === 'A') {
                    logger.info(`key A:\nmower ${action.mower_index}, old position: ${driver.mower.position.getXY()}, direction ${driver.mower.direction}`)
                    let destination_position = this.getNextPosition(driver.mower.position, driver.mower.direction)
                    if(this.destinationAreaInsideSurface(destination_position)) {
                        if (this.destinationAreaIsFree(destination_position)) {
                            this.updateStateAreas(driver.mower.position, destination_position)
                            driver.move()
                        } 
                    }
                } else if (keys[i] === 'G') {
                    logger.info(`key G:\nmower ${action.mower_index}, old position: ${driver.mower.position.getXY()}, direction ${driver.mower.direction}`) 
                    driver.turnLeft()  

                } else if (keys[i] === 'D') {
                    logger.info(`key D:\nmower ${action.mower_index}, old position: ${driver.mower.position.getXY()}, direction ${driver.mower.direction}`)
                    driver.turnRight()
                }          
                logger.info(`mower ${action.mower_index}, new position: ${driver.mower.position.getXY()}, direction ${driver.mower.direction} \n`)
                
            }
            this._mowers[action.mower_index] = driver.mower
    
            resolve(driver.mower)
        })
    }

    destinationAreaIsFree(position) {
       const destination_area = this._surface.areas.filter((area) => { return (area.position.pos_x == position.pos_x) && (area.position.pos_y == position.pos_y) })
       if(destination_area.length > 0) { return destination_area[0].state === State.FREE }
       else { return false }
    }

    destinationAreaInsideSurface(position) {
        return (position.pos_x <= this._surface._width) && (position.pos_y <= this._surface.height)
    }

    getNextPosition(position, direction) {
        const new_position = new Position(position.pos_x, position.pos_y)
        if(direction === Direction.NORTH) new_position.pos_y = ++new_position.pos_y 
        else if(direction === Direction.EAST) new_position.pos_x = ++new_position.pos_x
        else if(direction === Direction.SOUTH) new_position.pos_y = --new_position.pos_y
        else if(direction === Direction.WEST) new_position.pos_x = --new_position.pos_x
        return new_position
    }

    updateStateAreas(old_position, current_position) {
        const current_area = this._surface.areas.filter((area) => { return (area.position.pos_x == current_position.pos_x) && (area.position.pos_y == current_position.pos_y) })
        current_area[0].state = State.BUSY
        const old_area = this._surface.areas.filter((area) => { return (area.position.pos_x == old_position.pos_x) && (area.position.pos_y == old_position.pos_y) })
        old_area[0].state = State.FREE
    }

}
import Direction from '../models/mower/direction'

export default class MowerDriver {
   
    constructor(mower) {
        this._mower = mower
    }

    get mower() {
        return this._mower
    }

    set mower(value) {
        this._mower = value
    }

    move() {
        if(this._mower.direction === Direction.NORTH) this._mower.position.pos_y = ++this._mower.position.pos_y 
        else if(this._mower.direction === Direction.EAST) this._mower.position.pos_x = ++this._mower.position.pos_x
        else if(this._mower.direction=== Direction.SOUTH) this._mower.position.pos_y = --this._mower.position.pos_y
        else if(this._mower.direction === Direction.WEST) this._mower.position.pos_x = --this._mower.position.pos_x
    }

    turnRight() {
        if(this._mower.direction === Direction.NORTH) this._mower.direction = "E"
        else if(this._mower.direction === Direction.EAST) this._mower.direction = "S"
        else if(this._mower.direction === Direction.SOUTH) this._mower.direction = "W"
        else if(this._mower.direction === Direction.WEST) this._mower.direction = "N"
    }

    turnLeft() {
        if(this._mower.direction === Direction.NORTH) this._mower.direction = "W"
        else if(this._mower.direction === Direction.WEST) this._mower.direction = "S"
        else if(this._mower.direction === Direction.SOUTH) this._mower.direction = "E"
        else if(this._mower.direction === Direction.EAST) this._mower.direction = "N"  
    }
}
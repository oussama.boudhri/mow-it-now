import * as fs from 'fs'
import linereader from 'line-reader'
import logger from 'winston'

export default class FileProcessor {

    constructor(fileName) {
        this._fileName = fileName
    }

    get fileName () {
        return this._fileName
    }

    set fileName (value) {
        this._fileName = value
    }

    processFile() {
        return new Promise ((resolve, reject) => {
            let instructions = []
            logger.info("FileProcessor: begin processing file")
            let stream = fs.createReadStream(this._fileName)
            let index = 0
            linereader.eachLine(stream, (line) => {
                const chunks = line.trim().split(" ")
                if (chunks.length === 1) {
                    // move mower commands
                   instructions.push({ type: 'action', commands: chunks[0], mower_index: index++  })
                } else if(chunks.length === 2) {
                    // set most top right coordinate of surface
                   instructions.push({ type: 'init_surface', coordinates: {x: parseInt(chunks[0]) ,y: parseInt(chunks[1])} })
                } else if(chunks.length === 3) {
                    // set initial coordiate of the mower
                   instructions.push({ type: 'init_mower', coordinates: {x: parseInt(chunks[0]) ,y: parseInt(chunks[1]), direction: chunks[2] }})
                } 
            }, ()=> {resolve(instructions)})
        })
    }

}
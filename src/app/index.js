import FileProcessor from './services/file_processor'
import InstructionProcessor from './services/instruction_processor'

export default class App {
    start() {
        const file_processor = new FileProcessor(process.argv[2])
        file_processor.processFile().then((instructions) => {
            const instruction_processor = new InstructionProcessor(instructions)
            instruction_processor.processInstructions().then(() => {})
        })
    }

}

let app = new App()
app.start()
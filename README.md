# mow-it-now
[![pipeline status](https://gitlab.com/oussama.boudhri/mow-it-now/badges/master/pipeline.svg)](https://gitlab.com/oussama.boudhri/mow-it-now/commits/master)

App for an automatic mower, designed for rectangular surfaces. Starting from a file
containing commands for surface and mower initialisations, furthemore actions for
moving mower, your surface will be perfect.

# An example of command file:
```
5 5
1 2 N
GAGAGAGAA
3 3 E
ADAADADDA
5 2 S
AGAGAADAGA
```

# Explanations:
- First line indicate the top most right coordinates of the surface x=5, y=5
- second line contain the initial position of the first mower and its direction
- Third line contain a serie of commands for moving inside the surface
    <br/>Press D : turn mower 90 degree to the right direction
    <br/>Press G: turn mower 90 degree to the left direction
    <br/>Press A: advance mower one step without changing orientation


# Install packages
```
$ yarn
```
# Build project
```
$ gulp
```
# Launch app
```
$ npm run start "file_instructions/instructions.txt"
```
# Tests
```js
// Run tests
npm run test
```

## License
MIT © [Oussama Boudhri]

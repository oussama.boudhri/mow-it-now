import chai from 'chai'
import Surface from '../../app/models/surface'
chai.should()

let surface

beforeEach(() => { 
  surface = new Surface()
})

describe('Surface Area', () => {

  it(`should define most top right coordinate`, (done) => {
    surface.width = 5
    surface.height = 5
    surface.mostTopRightCoordinate().should.be.deep.equal([5,5])
    done()
  })

})
import chai from 'chai'
import Mower from '../../app/models/mower'
import Position from '../../app/models/commons/position'
chai.should()

let mower

beforeEach(() => { 
  const pos = new Position(0, 0)
  mower = new Mower(pos)
})

describe('Mower', () => {

  it(`should position be not null`, (done) => {
    should.not.equal(mower.position, null)
    done()
  })

   it(`should change position when set another value`, (done) => {
    const pos = new Position(1, 0)
    mower.position = pos
    mower.position.should.be.deep.equal(pos)
    done()
  })

})
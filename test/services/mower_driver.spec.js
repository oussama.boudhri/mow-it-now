import chai from 'chai'
import Mower from '../../app/models/mower'
import MowerDriver from '../../app/services/mower_driver'
import Position from '../../app/models/commons/position'
import Direction from '../../app/models/mower/direction'

chai.should()

let mower, driver

beforeEach(() => { 
  const pos = new Position(0, 0, "N")
  mower = new Mower(pos)
  driver = new MowerDriver(mower)
})

describe('Mower', () => {

  it(`should mower be not null`, (done) => {
    should.not.equal(driver.mower, null)
    done()
  })

  it(`should increase Y coordinate when moving in north direction`, (done) => {
    driver.mower.direction = Direction.NORTH
    driver.move()
    driver.mower.position.getXY().should.be.deep.equal([0,1])
    done()
  })

  it(`should increase X coordinate when moving in east direction`, (done) => {
    driver.mower.direction = Direction.EAST
    driver.move()
    driver.mower.position.getXY().should.be.deep.equal([1,0])
    done()
  })

  it(`should deacrease Y coordinate when moving in south direction`, (done) => {
    driver.mower.direction = Direction.SOUTH
    driver.move()
    driver.mower.position.getXY().should.be.deep.equal([0,-1])
    done()
  })

  it(`should deacrease X coordinate when moving in west direction`, (done) => {
    driver.mower.direction = Direction.WEST
    driver.move()
    driver.mower.position.getXY().should.be.deep.equal([-1,0])
    done()
  })

})
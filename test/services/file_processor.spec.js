import chai from 'chai'
import FileProcessor from '../../app/services/file_processor'
chai.should()

let processor

beforeEach(() => { 
  processor = new FileProcessor("config/commands/mower-movements.txt")
})

describe('Instruction Processor', () => {

  it(`should process file when path is correct`, (done) => {
     processor.processFile().should.be.not.rejected
    done()
  })

  it(`should throw exception when path is empty`, (done) => {
    processor.fileName = ""
    processor.processFile().should.be.rejected
    done()
  })

})
import chai from 'chai'
import InstructionProcessor from '../../app/services/instruction_processor'
chai.should()

let instruction_processor

beforeEach(() => { 
    const instructions = [ 
        { type: 'init_surface', coordinates: { x: 5, y: 5 } },
        { type: 'init_mower', coordinates: { x: 1, y: 2, direction: 'N' } },
        { type: 'action', commands: 'GAGAGAGAA', mower_index: 0 },
        { type: 'init_mower', coordinates: { x: 3, y: 3, direction: 'E' } },
        { type: 'action', commands: 'ADAADADDA', mower_index: 1 },
        { type: 'init_mower', coordinates: { x: 5, y: 2, direction: 'S' } },
        { type: 'action', commands: 'AGAGAADAGA', mower_index: 2 } ]
    instruction_processor = new InstructionProcessor(instructions)
  })

describe('Instruction processor', () => {
    
    it(`should mower be not null`, (done) => {
        instruction_processor.processInstructions().then((mowers) => {})
        done()
    })
    
})